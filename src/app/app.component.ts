import { Component } from '@angular/core';
import { resolve } from 'url';
import { reject } from 'q';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  nombre = 'saul Tejeda' ;

  nombre2 = 'SauL AntoNIo tEJeda eCheVERRia';

  arreglo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  pi = Math.PI;
  a = 0.23455465;
  salario = 12345.6;
  heroe = {
    nombre: 'Stark',
    clave: 'Iron man ',
    edad: 1000,
    direccion: {
      calle: 'primera ',
      casa: '19'
    }
  };

  // tslint:disable-next-line:no-shadowed-variable
  valorDePromesa = new Promise( ( resolve, reject ) => {

    setTimeout(() => resolve ('llego la data'), 3500);
  });

  fecha = new Date();

  video = 'spubd4HJGJU';

  activar = true;

}
